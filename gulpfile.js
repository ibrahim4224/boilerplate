// Gulp Components
const { series, parallel, src, dest, watch } = require("gulp");

// Image Minimization
const gulpImageMin = require("gulp-imagemin"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  sass = require("gulp-sass"),
  babel = require("gulp-babel"),
  bs = require("browser-sync"),
  imagemin = require("imagemin"),
  imageMinWebp = require("imagemin-webp"),
  jsonMinify = require("gulp-jsonminify");

// Site Absolute URL
const URL = "localhost/projects/boiletplate/",
  // Source Roots
  Source = {
    main: "src",
    images: "images",
    sass: "sass",
    js: "js",
    sassFiles: "pages/",
    fonts: "fonts"
  },
  // Compile Roots
  Compile = {
    main: "assets",
    css: "css",
    images: Source.images,
    fonts: Source.fonts,
    js: Source.js
  };

// Scss Compile
const scssCompile = () =>
  src([
    `${Source.main}/${Source.sass}/${Source.sassFiles}*`,
    `${Source.main}/${Source.sass}/fonts.scss`
  ])
    .pipe(
      sass({
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(dest(`${Compile.main}/${Compile.css}/`));

// Javascript Minify / Concatinating
const jsCompile = () =>
  src([
    `./${Source.main}/${Source.js}/1.external.js`,
    `./${Source.main}/${Source.js}/2.functions.js`
  ])
    .pipe(
      babel({
        presets: ["@babel/preset-env"]
      })
    )
    .pipe(uglify())
    .pipe(dest(`./${Compile.main}/${Compile.js}`))
    .pipe(
      concat("all.js", {
        newLine: ";\n"
      })
    )
    .pipe(dest(`./${Compile.main}/${Compile.js}`));

//Image Minimization
const imageMin = () => (
  src(`${Source.main}/${Source.images}/**/*`)
    //.pipe(imagemin())
    .pipe(
      gulpImageMin({
        interlaced: true,
        progressive: true,
        optimizationLevel: 5,
        svgoPlugins: [
          {
            removeViewBox: true
          }
        ]
      })
    )
    .pipe(dest(`./${Compile.main}/${Compile.images}`)),
  // Webp Image Min
  imagemin(
    [
      `${Source.main}/${Source.images}/*.{jpg,png,tiff,webp}`,
      `${Source.main}/${Source.images}/**/*.{jpg,png,tiff,webp}`
    ],
    `${Compile.main}/${Compile.images}/webp/`,
    {
      use: [
        imageMinWebp({
          quality: 50
        })
      ]
    }
  ).then(() => {
    console.log("Webp Images converted / optimized");
  })
);

// Copy Fonts
const copyFonts = () =>
  src(`./${Source.main}/${Source.fonts}/*`).pipe(
    dest(`./${Compile.main}/${Compile.fonts}`)
  );

// Manifest JSON
const manifestCompress = () =>
  src(`${Source.main}/${Source.js}/manifest.json`)
    .pipe(jsonMinify())
    .pipe(dest(`${Compile.main}/js`));

const tasks = () => (
  // Watch Different Files And Perform Related Task
  watch(`./${Source.main}/${Source.js}/*.js`, jsCompile),
  watch(
    [
      `./${Source.main}/${Source.sass}/**/**/*.scss`,
      `./${Source.main}/${Source.sass}/**/*.scss`,
      `./${Source.main}/${Source.sass}/*.scss`
    ],
    scssCompile
  ),
  watch(`./${Source.main}/${Source.font}/*`, copyFonts),
  watch(
    [
      `./${Source.main}/${Source.images}/*`,
      `./${Source.main}/**/${Source.images}/*`,
      `./${Source.main}/**/**/${Source.images}/*`
    ],
    imageMin
  ),
  watch(`./${Source.main}/${Source.js}/manifest.json`, manifestCompress)
);

// All Tasks
const live = () => (
  tasks(),
  // Browser Sync Initiate
  watch([
    "./*.php",
    "./**/*.php",
    "./**/**/.php",
    "./**/**/**/.php",
    `./${Source.main}/${Source.sass}/**/**/*.scss`,
    `./${Source.main}/${Source.sass}/**/*.scss`,
    `./${Source.main}/${Source.sass}/*.scss`
  ]).on("change", bs.reload),
  bs
    .init({
      proxy: URL
    })
    .reload({
      stream: true
    })
);

// Tasks
exports.live = series(live);
exports.tasks = series(tasks);
exports.scssCompile = series(scssCompile);
exports.jsCompile = series(jsCompile);
exports.copyFonts = series(copyFonts);
exports.imageMin = series(imageMin);
exports.manifestCompress = series(manifestCompress);

// Default Task
exports.default = parallel(
  jsCompile,
  scssCompile,
  copyFonts,
  imageMin,
  manifestCompress
);
