<?php
function getUriSegments()
{
    return explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
}
function getUriSegment($n)
{
    $segs = getUriSegments();
    return count($segs) > 0 && count($segs) >= ($n - 1) ? $segs[$n] : '';
}
$fileName = basename(getUriSegment(3), ".php");

($fileName === 'index' || $fileName === '') && $fileName = 'home';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>BoiletPlate</title>
    <meta name="description" content="BoiletPlate" />

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="./assets/images/favicon.png" type="image/png" />
    <link rel="mask-icon" href="https://www.topvoucherscode.co.uk/assets/images/topvoucher-logo.png">

    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, initial-scale=1.0">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#39b54a">

    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#39b54a">

    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#39b54a">
    <meta name="apple-mobile-web-app-title" content="TopVouchersCode.co.uk Vouchers and Coupons!">

    <!-- Application Name -->
    <meta name="application-name" content="TopVouchersCode.co.uk Vouchers and Coupons!">

    <!-- Open Graph -->
    <meta property="og:title" content="TopVouchersCode.co.uk Vouchers and Coupons!" />
    <meta property="og:description" content="Welcome to your favourite destination of voucher codes, promo codes, discount codes, & free shipping offers!" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.topvoucherscode.co.uk/" />
    <meta property="og:image" content="https://www.topvoucherscode.co.uk/assets/images/topvoucher-logo.png" />
    <meta property="og:site_name" content="TopVouchersCode.co.uk" />
    <meta property="og:video" content="https://www.youtube.com/watch?v=cxjT21T5yeQ" />

    <!-- Telephone Number For Google Detection in Website -->
    <meta name="format-detection" content="telephone=no" />

    <!-- Canonical Link -->
    <link href="https://www.topvoucherscode.co.uk" rel="canonical" />

    <style>
        <?php readfile("./assets/css/" . $fileName . ".css"); ?>
    </style>

    <!-- Manifest JSON -->
    <!-- <link rel="manifest" href="./assets/js/manifest.json"> -->

    <!-- Fonts Preloader -->
    <link rel="preload" href="./assets/css/fonts.css" as="style" onload="this.rel='stylesheet'">
</head>

<body>
    <!-- main wrapper <start> -->
    <main class="main">

        <!-- header <start> -->
        <header class="header">

        </header>
        <!-- header <end> -->