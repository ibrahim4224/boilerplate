"use-strict";

// Number
let n = 0;

// Body
const body = document.querySelector("body"),
  // Right Sticky Widgets
  stickyRightSide = () => {
    if ($(".wgts").outerHeight(true) > $(window).outerHeight(true)) {
      let storeText = 0;

      let columnShortHeight =
        $(window).outerHeight(true) - $(".wgts").outerHeight(true) - 20;

      columnShortHeight =
        $(".wgts").outerHeight(true) - 40 > $(window).outerHeight(true)
          ? columnShortHeight
          : 40;

      $(".wgts").css("top", `${columnShortHeight}px`);
    }
  },
  // Window Scroll Function
  scrollAnimation = (to, duration) => {
    let element = document.scrollingElement || document.documentElement,
      start = element.scrollTop,
      change = to - start,
      startDate = +new Date(),
      currentDate,
      currentTime,
      // t = current time
      // b = start value
      // c = change in value
      // d = duration
      easeInOutQuad = (t, b, c, d) => {
        t /= d / 2;
        if (t < 1) return (c / 2) * t * t + b;
        t--;
        return (-c / 2) * (t * (t - 2) - 1) + b;
      },
      animateScroll = () => {
        currentDate = +new Date();
        currentTime = currentDate - startDate;
        element.scrollTop = parseInt(
          easeInOutQuad(currentTime, start, change, duration)
        );
        if (currentTime < duration) {
          requestAnimationFrame(animateScroll);
        } else {
          element.scrollTop = to;
        }
      };
    animateScroll();
  },
  storeRating = (a, b, c) => {
    var d = "store=" + b + "&value=" + a + "&ip=" + c;
    $.post("/index.php?route=product/manufacturer/storeRating", d, function(a) {
      $("." + b).html(a), $(".ratingCalculator").text(a);
    });
    console.log("working");
  },
  // Trigger Click
  simulateClick = elem => {
    // Create our event (with options)
    var evt = new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
      view: window
    });
    // If cancelled, don't dispatch our event
    var canceled = !elem.dispatchEvent(evt);
  },
  // Window Disable Scroll Function
  stopBodyScrolling = () => {
    let width = "100vw";
    body.style.overflowY = "hidden";
    body.style.width = width;
  },
  // Null Function
  disableHandler = () => {
    return;
  },
  // Reset Window Scroll Function
  resetBodyAndHeader = () => {
    body.style = {
      width: "auto",
      overflowY: "auto"
    };
    body.removeEventListener(
      ["scroll", "touchmove", "mousewheel"],
      disableHandler()
    );
  };

// Home Menu Button Click Event
let hmbtn = document.querySelector(".hmbtn");
hmbtn &&
  hmbtn.addEventListener("click", e => {
    e.stopPropagation();
    e.preventDefault();
    document.querySelector(".main").classList.toggle("hmtgl");
    scrollAnimation(0, 500);
    document.querySelector(".main.hmtgl") !== null
      ? (stopBodyScrolling(), scrollAnimation(0, 500))
      : resetBodyAndHeader();
  });

// Window Load Function
(() => {
  // Window Resize Function
  window.onresize = () => {
    // Check if menu is toggled above 991 screen
    document.querySelector(".main.hmtgl") &&
      window.innerWidth > 991 &&
      resetBodyAndHeader();

    // Check if menu is toggled below 992 screen
    document.querySelector(".main.hmtgl") &&
      window.innerWidth < 992 &&
      (stopBodyScrolling(), scrollAnimation(0, 500));
  };
})();

// Mobile Menu Close On Body Click
let main = document.querySelector(".main");
main &&
  main.addEventListener("click", e => {
    main.classList.contains("hmtgl") && simulateClick(hmbtn);
  });

// Disable Menu Close on Menu Click
let menu = document.querySelector(".main .header .menu");
menu &&
  menu.addEventListener("click", e => {
    e.target.localName !== "a" && e.preventDefault(), e.stopPropagation();
  });

// Browser fully loaded state - Using ES6
document.addEventListener("readystatechange", function docStateChange(e) {
  // Image Lazy Load
  document.onscroll = () => {
    const header = document.querySelector(".header"),
      headerClass = header.classList;

    header && header.getBoundingClientRect().height < window.pageYOffset
      ? headerClass.add("hfixed")
      : headerClass.remove("hfixed");

    // Storing all image that have data-src attribute in variable
    let elements = document.querySelectorAll(
      "[data-srcset],[data-src], [data-bgimage]"
    );

    // Looping Stored images by length
    elements.forEach(element => {
      // Viewport Element Visible
      if (
        element.getBoundingClientRect().top - window.innerHeight <
        window.pageYOffset
      ) {
        // Image Element
        if (
          !element.dataset.hasOwnProperty("bgimage") &&
          element.dataset.hasOwnProperty("src")
        ) {
          element.setAttribute("src", element.dataset.src);
          element.removeAttribute("data-src");
        }

        // Picture Source Element
        else if (element.dataset.hasOwnProperty("srcset")) {
          element.setAttribute("srcset", element.dataset.srcset);
          element.removeAttribute("data-srcset");
        }

        // Background Image Element
        else {
          element.removeAttribute("data-bgimage");
        }
      }
    });
  };

  // Trigger Scrolling
  document.dispatchEvent(new Event("scroll"));
});

// Search Form Toggle
if (document.querySelector(".nbsch")) {
  document.querySelector(".nbsch").addEventListener("click", () => {
    document.querySelector(".shform").classList.toggle("active");
  });
}

// Navigation Bar Toggler
if (document.querySelector(".nbtgl")) {
  document.querySelector(".nbtgl").addEventListener("click", () => {
    document.querySelector(".navbar").classList.toggle("active");
  });
}

// Text Expander
if (document.querySelector(".xpandr")) {
  document.querySelectorAll(".xpandr").forEach(xpandr => {
    let textDefaultLenght = 133;

    if (xpandr.dataset.maxtext !== undefined) {
      textDefaultLenght = parseInt(xpandr.dataset.maxtext);
    }

    if (xpandr.textContent.length > textDefaultLenght) {
      let xpandrBtn = document.createElement("a");
      xpandrBtn.classList.add("xpnbtn");
      xpandrBtn.href = "javascript:;";
      xpandrBtn.textContent = "Show More";

      let xpandrContent = document.createElement("span");
      xpandrContent.textContent = xpandr.textContent.substring(
        textDefaultLenght
      );

      let xpandrContentWrap = document.createElement("span");
      xpandrContentWrap.classList.add("xpncnt");
      xpandrContentWrap.appendChild(xpandrContent);
      xpandrContentWrap.appendChild(xpandrBtn);

      xpandr.innerHTML = `${xpandr.textContent.substring(
        0,
        textDefaultLenght
      )}<span class="dots">...</span>`;

      xpandr.appendChild(xpandrContentWrap);

      xpandrBtn.addEventListener("click", () => {
        xpandr.classList.toggle("active");
        xpandrBtn.textContent =
          xpandrBtn.textContent === "Show More" ? "Show Less" : "Show More";
      });
    }
  });
}
