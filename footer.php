    <!-- Footer -->
    <footer class="footer">
    </footer>
    <!-- .......................................................... -->

    </main>
    <script async src="assets/js/all.js"></script>

    <!-- Schema - Organization, WebSite, WebPage -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@graph": [

                {
                    "@type": "Organization",
                    "name": "Top Vouchers Code",
                    "url": "https://www.topvoucherscode.co.uk/",
                    "sameAs": [
                        "https://www.facebook.com/topvoucherscode",
                        "https://www.instagram.com/topvoucherscode.co.uk/",
                        "https://twitter.com/Topvoucherscode",
                        "https://www.youtube.com/watch?v=cxjT21T5yeQ"
                    ]
                },

                {
                    "@type": "WebSite",
                    "url": "https://www.independent.co.uk/vouchercodes",
                    "potentialAction": {
                        "@type": "SearchAction",
                        "target": "https://www.independent.co.uk/vouchercodes/search?query={query}",
                        "query-input": "required name=query"
                    }
                },

                {
                    "@type": "WebPage",
                    "url": "https://www.independent.co.uk/vouchercodes",
                    "description": "Explore the latest vouchers, promo codes, discounts and deals from The Independent! Shop with us and take advantage of our offers for your favourite products!",
                    "name": "Promo codes, Discounts and Best Vouchers | The Independent",
                    "primaryImageOfPage": {
                        "@type": "ImageObject",
                        "contentUrl": "https://static.savings-united.com/medium/49276/file_name/The_Independent.png"
                    }
                }

            ]
        }
    </script>
</body>

</html>